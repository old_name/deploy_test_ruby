# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
#role :app, %w{deploy@188.226.227.32:23570}
#role :web, %w{deploy@188.226.227.32:23570}
#role :db,  %w{deploy@188.226.227.32:23570}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '188.226.227.32', user: 'deploy', roles: %w{web app}
